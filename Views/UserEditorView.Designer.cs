﻿using MvvmForms.ViewModels;
using MvvmForms.Controls;

namespace MvvmForms.Views
{
    partial class UserEditorView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserEditorView));
            this._tlpUser = new System.Windows.Forms.TableLayoutPanel();
            this._lblName = new System.Windows.Forms.Label();
            this._txbName = new System.Windows.Forms.TextBox();
            this._bsrcUserViewModels = new System.Windows.Forms.BindingSource(this.components);
            this._ckbIsAdmin = new System.Windows.Forms.CheckBox();
            this._ckbIsActive = new System.Windows.Forms.CheckBox();
            this._lblRole = new System.Windows.Forms.Label();
            this._txbRole = new System.Windows.Forms.RichTextBox();
            this._scBase = new System.Windows.Forms.SplitContainer();
            this._dgvUsers = new System.Windows.Forms.DataGridView();
            this._tsBase = new System.Windows.Forms.ToolStrip();
            this.NameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._bsrcUserEditorViewModel = new System.Windows.Forms.BindingSource(this.components);
            this._tsbRefresh = new MvvmForms.Controls.ToolStripCommandButton();
            this._tsbAddDummyUser = new MvvmForms.Controls.ToolStripCommandButton();
            this._tlpUser.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._bsrcUserViewModels)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._scBase)).BeginInit();
            this._scBase.Panel1.SuspendLayout();
            this._scBase.Panel2.SuspendLayout();
            this._scBase.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._dgvUsers)).BeginInit();
            this._tsBase.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._bsrcUserEditorViewModel)).BeginInit();
            this.SuspendLayout();
            // 
            // _tlpUser
            // 
            this._tlpUser.ColumnCount = 3;
            this._tlpUser.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this._tlpUser.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this._tlpUser.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._tlpUser.Controls.Add(this._lblName, 0, 0);
            this._tlpUser.Controls.Add(this._txbName, 1, 0);
            this._tlpUser.Controls.Add(this._ckbIsAdmin, 1, 1);
            this._tlpUser.Controls.Add(this._ckbIsActive, 2, 1);
            this._tlpUser.Controls.Add(this._lblRole, 0, 2);
            this._tlpUser.Controls.Add(this._txbRole, 1, 2);
            this._tlpUser.Dock = System.Windows.Forms.DockStyle.Fill;
            this._tlpUser.Location = new System.Drawing.Point(0, 0);
            this._tlpUser.Name = "_tlpUser";
            this._tlpUser.RowCount = 4;
            this._tlpUser.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
            this._tlpUser.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
            this._tlpUser.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
            this._tlpUser.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._tlpUser.Size = new System.Drawing.Size(407, 341);
            this._tlpUser.TabIndex = 0;
            // 
            // _lblName
            // 
            this._lblName.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this._lblName.AutoSize = true;
            this._lblName.Location = new System.Drawing.Point(3, 8);
            this._lblName.Name = "_lblName";
            this._lblName.Size = new System.Drawing.Size(81, 17);
            this._lblName.TabIndex = 0;
            this._lblName.Text = "User name:";
            // 
            // _txbName
            // 
            this._txbName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this._tlpUser.SetColumnSpan(this._txbName, 2);
            this._txbName.DataBindings.Add(new System.Windows.Forms.Binding("Text", this._bsrcUserViewModels, "Name", true));
            this._txbName.Location = new System.Drawing.Point(90, 6);
            this._txbName.Name = "_txbName";
            this._txbName.Size = new System.Drawing.Size(314, 22);
            this._txbName.TabIndex = 1;
            // 
            // _bsrcUserViewModels
            // 
            this._bsrcUserViewModels.DataMember = "Users";
            this._bsrcUserViewModels.DataSource = this._bsrcUserEditorViewModel;
            // 
            // _ckbIsAdmin
            // 
            this._ckbIsAdmin.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._ckbIsAdmin.AutoSize = true;
            this._ckbIsAdmin.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this._bsrcUserViewModels, "IsAdmin", true));
            this._ckbIsAdmin.Location = new System.Drawing.Point(90, 40);
            this._ckbIsAdmin.Name = "_ckbIsAdmin";
            this._ckbIsAdmin.Size = new System.Drawing.Size(66, 21);
            this._ckbIsAdmin.TabIndex = 2;
            this._ckbIsAdmin.Text = "Admin";
            this._ckbIsAdmin.UseVisualStyleBackColor = true;
            // 
            // _ckbIsActive
            // 
            this._ckbIsActive.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._ckbIsActive.AutoSize = true;
            this._ckbIsActive.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this._bsrcUserViewModels, "IsActive", true));
            this._ckbIsActive.Location = new System.Drawing.Point(162, 40);
            this._ckbIsActive.Name = "_ckbIsActive";
            this._ckbIsActive.Size = new System.Drawing.Size(65, 21);
            this._ckbIsActive.TabIndex = 3;
            this._ckbIsActive.Text = "Active";
            this._ckbIsActive.UseVisualStyleBackColor = true;
            // 
            // _lblRole
            // 
            this._lblRole.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this._lblRole.AutoSize = true;
            this._lblRole.Location = new System.Drawing.Point(14, 76);
            this._lblRole.Name = "_lblRole";
            this._lblRole.Size = new System.Drawing.Size(70, 17);
            this._lblRole.TabIndex = 5;
            this._lblRole.Text = "User role:";
            // 
            // _txbRole
            // 
            this._tlpUser.SetColumnSpan(this._txbRole, 2);
            this._txbRole.DetectUrls = false;
            this._txbRole.Dock = System.Windows.Forms.DockStyle.Fill;
            this._txbRole.Location = new System.Drawing.Point(90, 71);
            this._txbRole.Name = "_txbRole";
            this._tlpUser.SetRowSpan(this._txbRole, 2);
            this._txbRole.ShortcutsEnabled = false;
            this._txbRole.Size = new System.Drawing.Size(314, 267);
            this._txbRole.TabIndex = 6;
            this._txbRole.Text = "";
            // 
            // _scBase
            // 
            this._scBase.Dock = System.Windows.Forms.DockStyle.Fill;
            this._scBase.Location = new System.Drawing.Point(0, 27);
            this._scBase.Name = "_scBase";
            // 
            // _scBase.Panel1
            // 
            this._scBase.Panel1.Controls.Add(this._dgvUsers);
            // 
            // _scBase.Panel2
            // 
            this._scBase.Panel2.Controls.Add(this._tlpUser);
            this._scBase.Size = new System.Drawing.Size(616, 341);
            this._scBase.SplitterDistance = 205;
            this._scBase.TabIndex = 1;
            // 
            // _dgvUsers
            // 
            this._dgvUsers.AllowUserToAddRows = false;
            this._dgvUsers.AllowUserToDeleteRows = false;
            this._dgvUsers.AutoGenerateColumns = false;
            this._dgvUsers.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this._dgvUsers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._dgvUsers.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NameDataGridViewTextBoxColumn});
            this._dgvUsers.DataSource = this._bsrcUserViewModels;
            this._dgvUsers.Dock = System.Windows.Forms.DockStyle.Fill;
            this._dgvUsers.Location = new System.Drawing.Point(0, 0);
            this._dgvUsers.MultiSelect = false;
            this._dgvUsers.Name = "_dgvUsers";
            this._dgvUsers.ReadOnly = true;
            this._dgvUsers.RowTemplate.Height = 24;
            this._dgvUsers.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this._dgvUsers.Size = new System.Drawing.Size(205, 341);
            this._dgvUsers.TabIndex = 0;
            // 
            // _tsBase
            // 
            this._tsBase.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this._tsBase.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._tsbRefresh,
            this._tsbAddDummyUser});
            this._tsBase.Location = new System.Drawing.Point(0, 0);
            this._tsBase.Name = "_tsBase";
            this._tsBase.Size = new System.Drawing.Size(616, 27);
            this._tsBase.TabIndex = 2;
            this._tsBase.Text = "_tsBase";
            // 
            // NameDataGridViewTextBoxColumn
            // 
            this.NameDataGridViewTextBoxColumn.DataPropertyName = "Name";
            this.NameDataGridViewTextBoxColumn.HeaderText = "Name";
            this.NameDataGridViewTextBoxColumn.Name = "NameDataGridViewTextBoxColumn";
            this.NameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // _bsrcUserEditorViewModel
            // 
            this._bsrcUserEditorViewModel.DataSource = typeof(MvvmForms.ViewModels.UserEditorViewModel);
            // 
            // _tsbRefresh
            // 
            this._tsbRefresh.DataBindings.Add(new System.Windows.Forms.Binding("Command", this._bsrcUserEditorViewModel, "RefreshCommand", true));
            this._tsbRefresh.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this._tsbRefresh.Image = ((System.Drawing.Image)(resources.GetObject("_tsbRefresh.Image")));
            this._tsbRefresh.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._tsbRefresh.Name = "_tsbRefresh";
            this._tsbRefresh.Size = new System.Drawing.Size(62, 24);
            this._tsbRefresh.Text = "Refresh";
            // 
            // _tsbAddDummyUser
            // 
            this._tsbAddDummyUser.DataBindings.Add(new System.Windows.Forms.Binding("Command", this._bsrcUserEditorViewModel, "AddUserCommand", true));
            this._tsbAddDummyUser.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this._tsbAddDummyUser.Image = ((System.Drawing.Image)(resources.GetObject("_tsbAddDummyUser.Image")));
            this._tsbAddDummyUser.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._tsbAddDummyUser.Name = "_tsbAddDummyUser";
            this._tsbAddDummyUser.Size = new System.Drawing.Size(126, 24);
            this._tsbAddDummyUser.Text = "Add dummy user";
            // 
            // UserEditorView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(616, 368);
            this.Controls.Add(this._scBase);
            this.Controls.Add(this._tsBase);
            this.Name = "UserEditorView";
            this.Text = "User editor";
            this._tlpUser.ResumeLayout(false);
            this._tlpUser.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._bsrcUserViewModels)).EndInit();
            this._scBase.Panel1.ResumeLayout(false);
            this._scBase.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._scBase)).EndInit();
            this._scBase.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._dgvUsers)).EndInit();
            this._tsBase.ResumeLayout(false);
            this._tsBase.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._bsrcUserEditorViewModel)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel _tlpUser;
        private System.Windows.Forms.Label _lblName;
        private System.Windows.Forms.TextBox _txbName;
        private System.Windows.Forms.CheckBox _ckbIsAdmin;
        private System.Windows.Forms.CheckBox _ckbIsActive;
        private System.Windows.Forms.SplitContainer _scBase;
        private System.Windows.Forms.DataGridView _dgvUsers;
        private System.Windows.Forms.BindingSource _bsrcUserEditorViewModel;
        private System.Windows.Forms.ToolStrip _tsBase;
        private MvvmForms.Controls.ToolStripCommandButton _tsbRefresh;
        private MvvmForms.Controls.ToolStripCommandButton _tsbAddDummyUser;
        private System.Windows.Forms.Label _lblRole;
        private System.Windows.Forms.RichTextBox _txbRole;
        private System.Windows.Forms.BindingSource _bsrcUserViewModels;
        private System.Windows.Forms.DataGridViewTextBoxColumn NameDataGridViewTextBoxColumn;
    }
}

