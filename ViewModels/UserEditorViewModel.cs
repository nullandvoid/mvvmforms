﻿using System.ComponentModel;
using System.Linq;
using System.Windows.Input;
using MvvmForms.Models;

namespace MvvmForms.ViewModels
{
    /// <summary>
    /// ViewModel for the users list.
    /// </summary>
    class UserEditorViewModel : ViewModelBase
    {
        private BindingList<UserViewModel> _users;
        private RelayCommand _refreshCommand, _addUserCommand;

        /// <summary>
        /// Initializes a new instance of the <see cref="UserEditorViewModel" /> class.
        /// </summary>
        public UserEditorViewModel()
        {
            Refresh();
        }

        /// <summary>
        /// Gets all users.
        /// </summary>
        /// <value>
        /// The users.
        /// </value>
        public BindingList<UserViewModel> Users
        {
            get { return _users; }
            private set
            {
                if (_users == value)
                    return;
                _users = value;
                OnPropertyChanged("Users");
            }
        }

        /// <summary>
        /// Gets the refresh command.
        /// </summary>
        /// <value>
        /// The refresh command.
        /// </value>
        public ICommand RefreshCommand
        {
            get
            {
                _refreshCommand = _refreshCommand
                                  ?? new RelayCommand(param => Refresh());
                return _refreshCommand;
            }
        }

        /// <summary>
        /// Gets the add new user command.
        /// </summary>
        /// <value>
        /// The add user command.
        /// </value>
        public ICommand AddUserCommand
        {
            get
            {
                _addUserCommand = _addUserCommand
                                  ?? new RelayCommand(param => AddDummyUser(),
                                                      param => _users.Count < 10);
                return _addUserCommand;
            }
        }

        /// <summary>
        /// Reloads all existing users from the data repository.
        /// </summary>
        private void Refresh()
        {
            Users = new BindingList<UserViewModel>(
                User.GetUsers()
                    .Select(u => new UserViewModel(u))
                    .ToList());
            
            if (_addUserCommand != null)
            {
                _addUserCommand.RaiseCanExecuteChanged();
            }
        }

        /// <summary>
        /// Adds a dummy user to the users list. Not persistent!
        /// </summary>
        private void AddDummyUser()
        {
            var dummyUser = new User("User " + Users.Count);
            Users.Add(new UserViewModel(dummyUser));
            
            if (_addUserCommand != null)
            {
                _addUserCommand.RaiseCanExecuteChanged();
            }
        }
    }
}
